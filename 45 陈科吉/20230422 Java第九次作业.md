```java
//我们计划为一个电器销售公司制作一套系统，公司的主要业务是销售一些家用电器，例如：电冰箱、洗衣机、电视机产品。
//父类
package T1;

public class Electrica {
    private String brand;
    private String model;
    private String colour;
    private double price;

    public Electrica(String brand,String model,String colour,double price){
        this.brand = brand;
        this.model = model;
        this.colour = colour;
        this.price = price;
    }
    public Electrica(){

    }

    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setModel(String model){
        this.model = model ;
    }
    public void setColour(String colour){
        this.colour = colour;
    }
    public void setPrice(double price){
        this.price = price;
    }

    public String getBrand(){
        return this.brand = brand;
    }
    public String getModel(){
        return this.model = model;
    }
    public String getColour(){
        return this.colour = colour;
    }
    public double getPrice(){
        return this.price = price;
    }
}


//子类 冰箱
package T1;

public class Refrigerator extends Electrica {
    private String doorStyle;
    private String RefrigerationMethod;
    public Refrigerator(){

    }

    public void setDoorStyle(String doorStyle){
        this.doorStyle = doorStyle;
    }
    public void setRefrigerationMethod(String RefrigerationMethod){
        this.RefrigerationMethod = RefrigerationMethod;
    }

    public String getDoorStyle(){
        return this.doorStyle = doorStyle;
    }
    public String getRefrigerationMethod(){
        return this.RefrigerationMethod = RefrigerationMethod;
    }

    public Refrigerator(String doorStyle,String RefrigerationMethod){
        this.doorStyle = doorStyle;
        this.RefrigerationMethod = RefrigerationMethod;
    }

    public String toString(){
        return "冰箱的品牌为：" + super.getBrand() + "\n型号为：" + super.getModel() +
                "\n颜色为：" + super.getColour() + "\n售价为：" + super.getPrice() +
                "\n门款式为：" + getDoorStyle() + "\n制冷方式为：" + getRefrigerationMethod();
    }
}


//子类 洗衣机
package T1;

public class washingMachine extends Electrica{
    private String MotorType;
    private String WashingCapacity;
    public washingMachine(){

    }

    public void setMotorType(String MotorType){
        this.MotorType = MotorType;
    }
    public void setWashingCapacity(String WashingCapacity){
        this.WashingCapacity = WashingCapacity;
    }

    public String getMotorType(){
        return this.MotorType = MotorType;
    }
    public String getWashingCapacity(){
        return this.WashingCapacity = WashingCapacity;
    }
    public washingMachine(String MotorType,String WashingCapacity ){
        this.MotorType = MotorType;
        this.WashingCapacity = WashingCapacity;
    }

    public String toString(){
        return "洗衣机的品牌为：" + super.getBrand() + "\n型号为：" + super.getModel() +
                "\n颜色为：" + super.getColour() + "\n售价为：" + super.getPrice() +
                "\n电机类型为：" + getMotorType() + "\n洗涤容量为：" + getWashingCapacity();
    }
}


//子类 电视
package T1;

public class Television extends Electrica {
    private String screen;
    private String resolutionRatio;

    public Television(){

    }

    public void setScreen(String screen){
        this.screen = screen;
    }
    public void setResolutionRatio(String resolutionRatio){
        this.resolutionRatio = resolutionRatio;
    }

    public String getScreen(){
        return this.screen =screen;
    }
    public String getResolutionRatio(){
        return this.resolutionRatio = resolutionRatio;
    }

    public Television(String screen,String resolutionRatio){
        this.screen = screen;
        this.resolutionRatio = resolutionRatio;
    }

    public String toString(){
        return "电视的品牌为：" + super.getBrand() + "\n型号为：" + super.getModel() +
                "\n颜色为：" + super.getColour() + "\n售价为：" + super.getPrice() +
                "\n屏幕尺寸为：" + getScreen() + "\n分辨率为：" + getResolutionRatio();
    }
}


//测试
package T1;

public class Test {
    public static void main(String[] args) {
        //冰箱
        Refrigerator re = new Refrigerator("双开门","速冻");
        re.setBrand("华为");
        re.setModel("超大型");
        re.setColour("白色");
        re.setPrice(1000);
        System.out.println(re);
        System.out.println();

        //洗衣机
        washingMachine wa = new washingMachine("超大型","10T");
        wa.setBrand("苹果");
        wa.setModel("苹果No.1");
        wa.setColour("红色");
        wa.setPrice(1200);
        System.out.println(wa);
        System.out.println();

        //电视
        Television te = new Television("8k","1920x1080");
        te.setBrand("三星");
        te.setModel("三星No.1");
        te.setColour("黑色");
        te.setPrice(2200);
        System.out.println(te);
    }
}

```

```java
我们计划为一个动物园制作一套信息管理系统，根据与甲方沟通，我们归纳了有以下几种动物需要记录到系统中：
    鸟类：		 鹦鹉、猫头鹰、喜鹊
	哺乳类：	大象、狼、长颈鹿
	爬行类：	鳄鱼、蛇、乌龟
    //爷类
    package T2;

public class Animal {
     private String name;
     private String sex;

     public Animal(){

     }

     public void setName(String name){
         this.name = name;
     }
     public void setSex(String sex){
         this.sex = sex;
     }

     public String getName(){
         return this.name = name;
     }
     public String getSex(){
         return this.sex = sex;
     }
}


//父类 鸟类
package T2.Bird;

import T2.Animal;

public class Bird extends Animal {
    private String speedPerHour;
    private String colour;

    public Bird(){

    }

    public void setSpeedPerHour(String speedPerHour){
        this.speedPerHour = speedPerHour;
    }
    public void setColour(String colour){
        this.colour = colour;
    }

    public String getSpeedPerHour(){
        return this.speedPerHour = speedPerHour;
    }
    public String getColour(){
        return this.colour = colour;
    }
}


//鹦鹉类
package T2.Bird;

public class Parrot extends Bird{
    private String speak;
    public Parrot(){

    }
    public void setSpeak(String speak){
        this.speak = speak;
    }

    public String getSpeak(){
        return this.speak = speak;
    }

    public Parrot(String speak){
        this.speak = speak;
    }

    public String toString(){
        return "这只名叫" + super.getName() + "的" +  super.getColour() +
                "的"+ super.getSex() +"鹦鹉正在说：" + getSpeak() + "\n它飞行的速度为：" + getSpeedPerHour();
    }
}

//鹦鹉类
package T2.Bird;

public class Owl extends Bird{
    private String sleep;

    public Owl(){

    }
    public void setSleep(String sleep){
        this.sleep = sleep;
    }

    public String getSleep(){
        return this.sleep = sleep;
    }

    public Owl(String sleep){
        this.sleep = sleep;
    }

    public String toString(){
        return "这只名叫" + super.getName() + "的" + super.getColour() +
                "的"+ super.getSex() +"猫头鹰正在：" + getSleep() + "\n它飞行的速度为：" + getSpeedPerHour();
    }
}


//喜鹊
package T2.Bird;

public class Magpie extends Bird{
    private String speak;

    public Magpie(){

    }

    public void setSpeak(String speak){
        this.speak = speak;
    }

    public String getSpeak(){
        return this.speak = speak;
    }

    public Magpie(String speak){
        this.speak = speak;
    }

    public String toString(){
        return "这只名叫" + super.getName() + "的" +  super.getColour() +
                "的"+ super.getSex() +"喜鹊正在：" + getSpeak() + "\n它飞行的速度为：" + getSpeedPerHour();
    }
}


//测试
package T2.Bird;

public class TestBird {
    public static void main(String[] args) {
        Parrot p = new Parrot("你好呀");
        p.setName("小樱");
        p.setSex("公");
        p.setColour("红白相间");
        p.setSpeedPerHour("10米每秒");
        System.out.println(p);

        System.out.println();

        Owl o = new Owl("呼呼大睡");
        o.setName("小猫");
        o.setSex("母");
        o.setColour("黑色");
        o.setSpeedPerHour("20米每秒");
        System.out.println(o);

        System.out.println();

        Magpie m = new Magpie("吃小鱼干");
        m.setName("小白");
        m.setSex("母");
        m.setColour("黑色");
        m.setSpeedPerHour("15米每秒");
        System.out.println(m);
    }
}


//哺乳类 父类
package T2.Lactation;

import T2.Animal;

public class Lactation extends Animal {
    private String hairColour;

    public Lactation(String hairColour) {
        this.hairColour = hairColour;
    }

    public Lactation() {

    }

    public String getHairColour() {
        return hairColour;
    }

    public void setHairColour(String hairColour) {
        this.hairColour = hairColour;
    }
}


//大象
package T2.Lactation;

public class Elephant extends Lactation{
    private double height;

    public Elephant(double height) {
        this.height = height;
    }

    public Elephant() {
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "这个名为" + super.getName() + "的" + super.getHairColour() +super.getSex() +
                "大象" + "\n他的身高有：" + getHeight() + "米";
    }
}


//狼
package T2.Lactation;

public class Wolf extends Lactation{
    private String eat;

    public Wolf(String eat) {
        this.eat = eat;
    }

    public Wolf() {
    }

    public String getEat() {
        return eat;
    }

    public void setEat(String eat) {
        this.eat = eat;
    }

    @Override
    public String toString() {
        return "这个名为" + super.getName() + "的" + super.getHairColour() +super.getSex() +
                "狼" + "\n正在吃：" + getEat() ;
    }
}


//长颈鹿
package T2.Lactation;

public class Giraffe extends Lactation{
    private String height;

    public Giraffe(String height) {
        this.height = height;
    }

    public Giraffe() {
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }


    @Override
    public String toString() {
        return "这个名为" + super.getName() + "的" + super.getHairColour() +super.getSex() +
                "长颈鹿" + "\n他的脖子有：" + getHeight() + "长" ;
    }
}


//测试
package T2.Lactation;

public class TestLactation {
    public static void main(String[] args) {
        Elephant el = new Elephant(5);
        el.setName("小象");
        el.setSex("公");
        el.setHairColour("黑色");
        System.out.println(el);

        System.out.println();

        Wolf w = new Wolf("野生肉");
        w.setName("黑狼");
        w.setSex("公");
        w.setHairColour("白色");
        System.out.println(w);

        System.out.println();

        Giraffe gi = new Giraffe("7");
        gi.setName("小鹿");
        gi.setSex("母");
        gi.setHairColour("黄色");
        System.out.println(gi);
    }
}

```

