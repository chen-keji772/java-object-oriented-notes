```java
package Demo1;

public class Graphic {
    public double area(){
        return 0.0;
    }

    public double perimeter(){
        return 0.0;
    }

    public String getInfo(){
        return "面积为：" + area() + ",周长为：" + perimeter();
    }
}

class Circle extends Graphic{
    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String getInfo() {
        return "面积为：" + area() + ",周长为：" + perimeter();
    }
}
class Rectangle extends Graphic{
    private double length;
    private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double area() {
        return length * width;
    }

    @Override
    public double perimeter() {
        return 2 * (length + width);
    }

    @Override
    public String getInfo() {
        return "面积为：" + area() + ",周长为：" + perimeter();    }
}


class Test{
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setRadius(2);
        System.out.println(circle.area());
        System.out.println(circle.perimeter());
        Rectangle rectangle = new Rectangle();
        rectangle.setLength(5);
        rectangle.setWidth(4.5);
        System.out.println(rectangle.area());
        System.out.println(rectangle.perimeter());
        message(circle,rectangle);
        message2(circle,rectangle);
    }

    public static void message(Graphic a,Graphic b){
        if (a.area() >=  b.area()){
            System.out.println("a >= b");
        }else {
            System.out.println("a <= b");
        }
    }

    public static void message2(Graphic a,Graphic b){
        if (a.perimeter() >= b.perimeter()){
            System.out.println("a >= b");
        }else {
            System.out.println("a <= b");
        }
    }
}
```

