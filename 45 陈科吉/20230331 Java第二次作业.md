~~~java
package sc1;
//    ## 1、强制类型转换练习
//
//（1）先声明两个byte类型的变量b1,b2,并分别赋值为10和20，求b1和b2变量的和，并将结果保存在byte类型的变量b3中，
// 最后输出b3变量的值
//
//（2）先声明两个short类型的变量s1,s2,并分别赋值为1000和2000，求s1和s2变量的和，并将结果保存在short类型的变量s3中，
// 最后输出s3变量的值
//
//（3）先声明1个char类型的变量c1赋值为'a'，再声明一个int类型的变量num赋值为5，求c1和num变量的和，
// 并将结果将结果保存在char类型的变量letter中，最后输出letter变量的值。
//
// (4）先声明两个int类型的变量i1,i2，并分别赋值5和2，求i1和i2的商，并将结果保存在double类型的变量result中，
// 最后输出result变量的值。如何得到结果2.5呢？
public class sc1 {
    public static void main(String[] args) {
        byte b1 = 10;
        byte b2 = 20;
        byte b3 = (byte)(b1 + b2);
        System.out.println("他们的和为：" + b3);

        short s1 = 1000;
        short s2 = 2000;
        short s3 = (short)(s1 + s2);
        System.out.println("他们的和为：" + s3);

        char c1 = 'a';
        int num = 5;
        char letter = (char)(c1 + 5);
        System.out.println("他们的和为：" + letter);

        int i1 = 5;
        int i2 = 2;
        double result = ((double) i1 / i2) ;
        System.out.println("他们的商为：" + result);
    }
}


package sc1;
//    1. 定义两个int类型变量a1和a2,分别赋值10,11,判断变量是否为偶数,拼接输出结果
//    2. 定义两个int类型变量a3和a4,分别赋值12,13,判断变量是否为奇数,拼接输出结果
public class sc2 {
    public static void main(String[] args) {
        int a1 = 10;
        int a2 = 11;
        if (a1 % 2 ==0){
            System.out.println("10是偶数？" + true);
        }
        if (a2 % 2 == 0){
            System.out.println("11是偶数？" + true);
        }else
            System.out.println("11是偶数？" + false);

        int a3 = 12;
        int a4 = 13;
        if (a3 % 2 ==0){
            System.out.println("12是偶数？" + true);
        }
        if (a4 % 2 == 0){
            System.out.println("13是偶数？" + true);
        }else
            System.out.println("13是偶数？" + false);
    }
}


package sc1;
//    1. 定义一个int类型变量hours，赋值为89
//2. 定义一个int类型变量day，用来保存89小时中天数的结果
//3. 定义一个int类型变量hour，用来保存89小时中不够一天的剩余小时数的结果
//4. 输出结果

public class sc3 {
    public static void main(String[] args) {
        int hours = 89;
        int day = 89 / 24;
        int hour = 89 % 24;
        System.out.println("为抵抗洪水，战士连续作战89小时:");
        System.out.println("89天是" + day + '天' + hour + "小时");
    }
}


package sc1;
//    1. 定义一个int类型变量week，赋值为2
//2. 修改week的值，在原值基础上加上100
//3. 修改week的值，在原值基础上模以7
//4. 输出结果，在输出结果的时候考虑特殊值，例如周日
public class sc4 {
    public static void main(String[] args) {
       int week = 2;
       System.out.print("今天是周"+week+',');
       week = week + 100 ;
        System.out.print(100+"天以后是");
       week = week % 7;
        System.out.print("周"+week);

    }
}


package sc1;
//    1. 定义三个int类型变量,x,y,z，随意赋值整数值
//    2. 定义一个int类型变量max，先存储x与y中的最大值（使用三元运算符）
//    3. 再次对max赋值，让它等于上面max与z中的最大值（使用三元运算符）
//    4. 输出结果
public class sc5 {
    public static void main(String[] args) {
        int x = (int)(Math.random()*(10000-0));
        int y = (int)(Math.random()*(10000-0));
        int z = (int)(Math.random()*(10000-0));
        int max;
        max = x > y ? x : y;
        int maxx = max > z ? max : z ;
        System.out.println((x)+"," + (y)+"," + (z)+"," + "最大值为："+ max);
    }
}


package sc1;
//    闰年的判断标准是：
//
//              1）可以被4整除，但不可被100整除
//
//  2）可以被400整除
//
//
//   1. 定义一个int类型变量year，赋值为今年年份值
//   2. 定一个一个boolean类型变量，用来保存这个年份是否是闰年的结果
//   3. 输出结果
import java.util.Scanner;
public class sc6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入年份判断是不是闰年");
        int year = sc.nextInt();
        if ((year % 4 == 0 && year % 100 !=0) || year % 400 ==0 ){
            System.out.println(year + "是闰年");
        }else {
            System.out.println(year + "不是闰年");
        }
    }
}


package sc1;
//    1. 定义一个double类型变量hua，存储华氏温度80
//2. 定义一个double类型变量she，存储摄氏温度，根据公式求值
//3. 输出结果
public class sc7 {
    public static void main(String[] args) {
     double hua = 80;
     double she = (hua - 32) / 1.8;
     System.out.println("华氏度80.0°F转化摄氏度是" + she);
    }
}


# 拔高题

## 第一题

```java
如下代码的计算结果是：
int i = 1;
i *= 0.2;  
i++;
System.out.println("i=" + i);

i=1
```

## 第二题

```java
如下代码的运算结果是：
int i = 2;
i *= i++;

int j = 2;
j *= j+1; 

int k = 2;
k *= ++k;

System.out.println("i=" + i);
System.out.println("j=" + j);
System.out.println("k=" + k);


i=4
j=6
k=6
```

## 第三题

```java
如下代码的运算结果是：
int a = 3;
int b = 1;
if(a = b){
	System.out.println("Equal");
}else{
	System.out.println("Not Equal");
}

Not Equal
```

## 第四题

```java
如下代码的运算结果是：
int a = 8, b = 3;
System.out.println(a>>>b);
System.out.println(a>>>b | 2);


1
3
```

## 第五题

如何用最有效的的方法计算2乘以8

```java
package sc1;

public class sc8 {
    public static void main(String[] args) {
        int num = 2 * 8;
        System.out.println(num);
    }
}
```


~~~

